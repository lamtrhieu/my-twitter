import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';

import loginReducer from './login/reducer';
import appReducer from './app/reducer';

import App from './app/App';


// const testReducer = (state = {}, action) => {
//     switch(action.type) {
//         case 'ABC': 
//             return {string: 'ABC'};
//         default: 
//             return state;
//     }
// }



const rootReducers = combineReducers({loginReducer, appReducer});


const store = createStore(
    rootReducers, 
    applyMiddleware(thunkMiddleware)
);

ReactDOM.render(
    <Provider store={store}> 
        <App />
    </Provider>    
, 
document.getElementById('root'));