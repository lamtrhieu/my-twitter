import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


class HomePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};
    }

    handleChange = (event) => {
        this.setState({value: event.target.value});
    }

    handleSubmit = (event) => {
        
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">What are you thinking ?</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" onChange={this.handleChange}></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Tweet</button>
                </form>

                <h3>Your tweets</h3>
                <ul class='list-group'> 
                    <li class="list-group-item">Tweet 1 : ksdjfl;sajfl;sjal;dfjsal;jf;lsjdlfjsa</li>
                    <li class="list-group-item">Tweet 1 : ksdjfl;sajfl;sjal;dfjsal;jf;lsjdlfjsa</li>
                    <li class="list-group-item">Tweet 1 : ksdjfl;sajfl;sjal;dfjsal;jf;lsjdlfjsa</li>
                </ul>


            </div>
        );
    }
}

export default HomePage;