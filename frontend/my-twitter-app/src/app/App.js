import React from 'react';
import { connect } from 'react-redux';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import LoginPage from '../login/LoginPage';
import { PrivateRoute } from './PrivateRoute'
import HomePage from '../homepage/HomePage';
import { history } from '../history/history'


class App extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    const { alert } = this.props;

    return (
        <div className="jumbotron">
            <div className="container">
                <div className="col-sm-8 col-sm-offset-2">
                    {/* {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                    } */}
                    <Router history={history}>
                        <Switch>
                            <PrivateRoute exact path="/" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                            <Redirect from="*" to="/" />
                        </Switch>
                    </Router>
                </div>
            </div>
        </div>
    );
  }
  
}

const mapStateToProps = (state) => ({
  alert: state.alert
})

const mapDispatchToProps = (dispatch) => ({
  clearAlert: null
})


export default connect(mapStateToProps, mapDispatchToProps)(App);
