import { resolveNaptr } from "dns";

const API_URL = 'http://localhost:8080/api/v1'

const clientId = 'app-client';
const clientSecret = 'app-secret';

export const login = async (username, password) => {
    const formData = new FormData();
    formData.append('grant_type', 'password');
    formData.append('username', username);
    formData.append('password', password);
    
    const headers = new Headers();
    headers.append('Authorization', 'Basic ' + btoa(clientId + ":" + clientSecret));

    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: formData
    };
    try {
        const response = await fetch('http://localhost:8080/oauth/token', requestOptions);
        if (!response.ok) {
            throw new Error('Network Response not ok');
        }
        const data = await response.json();

        console.log('save token to local storage', data);
        
        localStorage.setItem('token', data);
        
        return data;
    } catch (error) {
        throw new Error("There is problem with fetch");
    }    
}

const authHeader = () => {
    let token = localStorage.getItem('token');

    if (token && token.access_token) {
        return { 'Authorization': 'Bearer ' + token.access_token };
    }

    return {};

}

export const postTweet = async (tweet) => {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()        
    };

    return fetch(`API_URL/tweets`)
}