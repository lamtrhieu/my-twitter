package io.hieulam.mytwitterbackend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UserServiceImplTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void loadUserByUsername() {
        UserServiceImpl impl = new UserServiceImpl();

        UserDetails userDetails1 = impl.loadUserByUsername("user1");

        assertThat(userDetails1).isNotNull();
        assertThat(userDetails1.getUsername()).isEqualTo("user1");

        UserDetails userDetails2 = impl.loadUserByUsername("user2");

        assertThat(userDetails2).isNotNull();
        assertThat(userDetails2.getUsername()).isEqualTo("user2");
    }
}