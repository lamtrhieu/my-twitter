package io.hieulam.mytwitterbackend.controller;

import io.hieulam.mytwitterbackend.model.Otp;
import io.hieulam.mytwitterbackend.model.Tweet;
import io.hieulam.mytwitterbackend.repository.MyTwitterRepository;
import io.hieulam.mytwitterbackend.service.OtpService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.token.TokenStore;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MyTwitterControllerTest {

    @Mock
    MyTwitterRepository myTwitterRepository;

    @Mock
    private TokenStore tokenStore;

    @Mock
    private OtpService otpService;

    private MyTwitterController myTwitterController;

//    @BeforeAll
//    public static void beforeAll() {
//
//    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        myTwitterController = new MyTwitterController(myTwitterRepository, tokenStore, otpService);
    }

    @Test
    void generateOtp() {
        Otp otp = new Otp();
        otp.setValue("1234");
        when(otpService.generateOtp()).thenReturn(otp);

        String result = myTwitterController.generateOtp();

        assertThat(result).isEqualTo("1234");
    }

    @Test
    void validateOtp() {
        when(otpService.validateOtp("1234")).thenReturn(true);

        boolean result = myTwitterController.validateOtp("1234");

        assertThat(result).isTrue();

        when(otpService.validateOtp("1234")).thenReturn(false);
        result = myTwitterController.validateOtp("1234");
        assertThat(result).isFalse();

    }

    @Test
    void postTweet() {
        SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        Authentication authentication = mock(Authentication.class);
        when(authentication.getName()).thenReturn("user1");

        when(securityContext.getAuthentication()).thenReturn(authentication);

        Tweet value = new Tweet();
        when(myTwitterRepository.saveTweet("user1", "This is a tweet")).thenReturn(value);

        ResponseEntity<Tweet> result = myTwitterController.postTweet("This is a tweet");

        verify(myTwitterRepository).saveTweet("user1", "This is a tweet");
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).isEqualTo(value);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);


    }

//    @Test
//    void findTweetsByUser() {
//    }
//
//    @Test
//    void logout() {
//    }
}