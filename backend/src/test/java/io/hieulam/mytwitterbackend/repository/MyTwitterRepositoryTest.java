package io.hieulam.mytwitterbackend.repository;

import io.hieulam.mytwitterbackend.model.Tweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MyTwitterRepositoryTest {

    private MyTwitterRepository myTwitterRepository;

    @BeforeEach
    void setUp() {
        myTwitterRepository = new MyTwitterRepository();
    }

    @Test
    void findTweetsByUser() {
        myTwitterRepository.saveTweet("user1", "Test content");


        List<Tweet> tweets = myTwitterRepository.findTweetsByUser("user1");

        assertThat(tweets).hasSize(1);
        assertThat(tweets.get(0).getContent()).isEqualTo("Test content");
    }

    @Test
    void saveTweet() {
        Tweet tweet1 = myTwitterRepository.saveTweet("user1", "Test content");

        assertThat(tweet1.getContent()).isEqualTo("Test content");
        assertThat(tweet1.getOwner()).isEqualTo("user1");

        Tweet tweet2 = myTwitterRepository.saveTweet("user1", "Test content2");
        assertThat(tweet2.getContent()).isEqualTo("Test content2");
        assertThat(tweet2.getOwner()).isEqualTo("user1");

        assertThat(myTwitterRepository.findTweetsByUser("user1")).hasSize(2);

    }
}