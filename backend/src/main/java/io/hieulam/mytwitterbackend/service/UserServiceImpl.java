package io.hieulam.mytwitterbackend.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService {

    private Map<String, User> userMap;


    public UserServiceImpl() {
        this.userMap = new ConcurrentHashMap<>();

        User seedUser1 = new User("user1", encryptPassword("pass1"), Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
        User seedUser2 = new User("user2", encryptPassword("pass2"), Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
        userMap.put("user1", seedUser1);
        userMap.put("user2", seedUser2);

    }

    private String encryptPassword(String rawPass) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(rawPass);

        return encode;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMap.get(username);

        if (user != null) {
            return user;
        }

        throw new UsernameNotFoundException("Invalid username or password.");
    }

}
