package io.hieulam.mytwitterbackend.service;

import io.hieulam.mytwitterbackend.model.Otp;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class OtpService {

    private Map<String, Otp> otpMap;

    public OtpService() {
        this.otpMap = new ConcurrentHashMap<>();
    }

    public Otp generateOtp() {
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        ZonedDateTime creationDateTime = ZonedDateTime.now();
        ZonedDateTime expirationDateTime = creationDateTime.plusMinutes(5*60*1000);

        Otp otpObject = new Otp(String.valueOf(otp), creationDateTime, expirationDateTime);
        otpMap.put(String.valueOf(otp), otpObject);

        return otpObject;
    }

    public Otp getOtp(String username) {
        return otpMap.get(username);
    }

    public void clearOtp(String username) {
        otpMap.remove(username);
    }

    public boolean validateOtp(String otp) {
        Otp otpObject = otpMap.get(otp);

        if (otpObject == null) {
            return false;
        }

        ZonedDateTime now = ZonedDateTime.now();
        if (otpObject.getExpirationDateTime().isAfter(now)) {
            return false;
        }

        return true;
    }


}
