package io.hieulam.mytwitterbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Otp {
    String value;
    ZonedDateTime expirationDateTime;
    ZonedDateTime creationDateTime;
}
