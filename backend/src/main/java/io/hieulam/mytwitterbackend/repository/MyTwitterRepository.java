package io.hieulam.mytwitterbackend.repository;

import io.hieulam.mytwitterbackend.model.Tweet;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class MyTwitterRepository {

    private Map<String, List<Tweet>> userTweetsMap;

    public MyTwitterRepository() {
        userTweetsMap = new ConcurrentHashMap<>();
    }

    public List<Tweet> findTweetsByUser(String user) {
        return getUserTweets(user);
    }

    public Tweet saveTweet(String user, String content) {
        Tweet tweet = new Tweet();
        tweet.setContent(content);
        tweet.setCreationDate(ZonedDateTime.now());
        tweet.setId(UUID.randomUUID().toString());
        tweet.setOwner(user);

        List<Tweet> userTweets = getUserTweets(user);
        userTweets.add(tweet);

        return tweet;
    }

    private List<Tweet> getUserTweets(String user) {
        List<Tweet> tweets = userTweetsMap.get(user);
        if (tweets == null) {
            tweets = new ArrayList<>();
            userTweetsMap.put(user, tweets);
        }

        return tweets;
    }

//    public Optional<User> findUserById(String userId) {
//        Set<User> users = userTweetsMap.keySet();
//
//        return users.stream().filter(user -> user.getUsername().equals(userId)).findFirst();
//    }
}
