package io.hieulam.mytwitterbackend.controller;

import io.hieulam.mytwitterbackend.model.Tweet;
import io.hieulam.mytwitterbackend.repository.MyTwitterRepository;
import io.hieulam.mytwitterbackend.service.MyTwitterService;
import io.hieulam.mytwitterbackend.service.OtpService;
import jdk.nashorn.internal.parser.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class MyTwitterController {

    @Autowired
    MyTwitterRepository myTwitterRepository;

    @Autowired
    TokenStore tokenStore;

    @Autowired
    OtpService otpService;

    public MyTwitterController(MyTwitterRepository myTwitterRepositorytory, TokenStore tokenStore, OtpService otpService) {
        this.myTwitterRepository = myTwitterRepositorytory;
        this.tokenStore = tokenStore;
        this.otpService = otpService;
    }

    @PostMapping("/generateOtp")
    @ResponseBody
    public String generateOtp() {
        return otpService.generateOtp().getValue();
    }

    @PostMapping("/validateOtp")
    @ResponseBody
    public boolean validateOtp(@RequestParam String otp) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        System.out.println("user name " + authentication.getName());

        return otpService.validateOtp(otp);
    }


    @PostMapping("/tweets")
    @ResponseBody
    public ResponseEntity<Tweet> postTweet(@RequestBody String content) {
        Tweet result = null;

        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String userId = authentication.getName();

            result = myTwitterRepository.saveTweet(userId, content);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/tweets")
    public ResponseEntity<List<Tweet>> findTweetsByUser() {
        List<Tweet> result = null;
        try {
             Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
             String userId = authentication.getName();

             result = myTwitterRepository.findTweetsByUser(userId);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/logout")
    public void logout(@RequestBody String token) {
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
        tokenStore.removeAccessToken(accessToken);
    }




}
