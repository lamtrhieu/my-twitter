package io.hieulam.mytwitterbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyTwitterBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTwitterBackendApplication.class, args);
    }

}
